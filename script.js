class Hamburger {
    static sizeArray = [{
        name: "small",
        type: "size",
        price: 50,
        calorie: 20,
    },

        {
            name: "large",
            type: "size",
            price: 100,
            calorie: 40,
        }
    ];
    static stuffingArray = [
        {
            name: "cheese",
            type: "stuffing",
            price: 10,
            calorie: 20,
        },
        {
            name: "salad",
            type: "stuffing",
            price: 20,
            calorie: 5,
        },
        {
            name: "potato",
            type: "stuffing",
            price: 15,
            calorie: 10,
        }
    ];
    static toppingsArray = [
        {
            name: "mayo",
            type: "topping",
            price: 20,
            calorie: 5,
        },
        {
            name: "spice",
            type: "topping",
            price: 15,
            calorie: 0,
        }
    ];

    constructor(size, stuffing) {
        this._topping = [];
        try {
            if (!size || (size !== 'small' && size !== 'large')) {
                throw  new Error('Size is expected. Enter correct type of size (small or large)!');
            } else {
                this._size = Hamburger.sizeArray.find(function (item) {
                    return item.name === size;

                })
            }
        } catch (err) {
            console.log(err.message)
        }
        try {
            if (!stuffing || (stuffing !== "cheese" && stuffing !== 'potato' && stuffing !== 'salad')) {
                throw new Error('Stuffing is expected! Enter correct type of stuffing (cheese/potato/salad)!');
            } else {
                this._stuffing = Hamburger.stuffingArray.find(function (item) {
                    return item.name === stuffing;
                });
            }
        } catch (err) {
            console.log(err.message)
        }
    }

    set addToppings(topping) {
        try {
            if (!topping || (topping !== 'mayo' && topping !== 'spice')) {
                throw  new Error('Please, enter topping correctly (mayo/spice)!');
            } else {
                if (this._topping.length > 0) {
                    for (let i = 0; i < this._topping.length; i++) {
                        if (this._topping[i].name === topping) {
                            throw new Error('This type of topping has been already existed')
                        }
                    }
                }
                this._topping.push(Hamburger.toppingsArray.find(function (item) {
                    return item.name === topping
                }))

            }

        } catch
            (err) {
            console.log(err.message)

        }
    }


    get toppings() {
        console.log('Toppings of new burger:', this._topping);
        return this._topping
    }

    set removeToppings(topping) {
        try {
            if (!topping || (topping !== 'mayo' && topping !== 'spice')) {
                throw  new Error('Please, enter topping correctly (mayo/spice)!');
            }
            if (this._topping.length === 0) {
                throw new Error('Toppings were not added');
            } else {
                for (let i = 0; i <= this._topping.length; i++) {
                    if (this._topping[i].name === topping) {
                        this._topping.splice(i, 1);
                    } else {
                        throw new Error('Toppings were not added');
                    }

                    return this._topping

                }
            }
        } catch (err) {
            console.log(err.message)
        }

    }


    calculatePrice() {
        return `Price is ${(this._size.price + this._stuffing.price +
            (this._topping.reduce((total, item) => total + item.price, 0)))}`
    }

    calculateCalories() {
        return `Calories are ${(this._size.calorie + this._stuffing.calorie +
            (this._topping.reduce((total, item) => total + item.calorie, 0)))}`
    }


}


let newBurger = new Hamburger('large', 'potato');
console.log(newBurger);

newBurger.addToppings = 'mayo';
newBurger.addToppings = 'spice';
newBurger.toppings;
newBurger.removeToppings = 'mayo';



console.log(newBurger.calculateCalories());
console.log(newBurger.calculatePrice());








